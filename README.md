# diagramGRmodels

Diagrams of the GR hydrological models developped by the [Hydrology Team - INRAE (Antony)](https://webgr.inrae.fr/eng)

The file, readable with the Inkscape software, currently contains the schematics of the following models:

- GRP
- GR2M
- GR4J
- GR5J
- GR6J
- GR5H
 
The texts are available in English and French.

To choose the template and the language, simply select the appropriate layers in the layer management panel by clicking on the 'eye' icon (N.B.: a layer can be common to several templates).

By default, layers are protected.

To make changes to a layer (e.g. colors), remove its protection by clicking on the 'padlock' icon in the layer management panel.

It is easy to export images in PDF or PNG formats.

